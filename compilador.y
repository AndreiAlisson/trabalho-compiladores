
// Testar se funciona corretamente o empilhamento de par�metros
// passados por valor ou por refer�ncia.


%{
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "compilador.h"

int num_vars;

%}

%token PROGRAM ABRE_PARENTESES FECHA_PARENTESES 
%token VIRGULA PONTO_E_VIRGULA DOIS_PONTOS PONTO
%token T_BEGIN T_END VAR IDENT ATRIBUICAO
%token LABEL TYPE ARRAY OF PROCEDURE FUNCTION GOTO IF THEN ELSE WHILE DO NOT DIV AND OR

%%

programa    :{ 
             geraCodigo (NULL, "INPP"); 
             }
             PROGRAM IDENT 
             ABRE_PARENTESES lista_idents FECHA_PARENTESES PONTO_E_VIRGULA
             bloco PONTO {
             geraCodigo (NULL, "PARA"); 
             limpa_TS();
             }
;

bloco       : 
              parte_declara_vars
              { 
              }

              comando_composto 
              ;




parte_declara_vars:  parte_declara_vars var | var 
;


var         : { num_vars = 0; } VAR declara_vars
            |
;

declara_vars: declara_vars declara_var 
            | declara_var 
;

declara_var : {  } 
              lista_id_var DOIS_PONTOS 
              tipo 
              { 
                char str[15];
                sprintf(str, "AMEM %d", num_vars);
                geraCodigo (NULL, str);
              }
              PONTO_E_VIRGULA
;

tipo        : IDENT
;

lista_id_var: lista_id_var VIRGULA IDENT 
              { insere_TS(token, "VS", 0, ++num_vars, "integer"); }
            | IDENT { insere_TS(token, "VS", 0, num_vars++, "integer");}
;

lista_idents: lista_idents VIRGULA IDENT  
            | IDENT
;


comando_composto: T_BEGIN comandos T_END 

comandos: comandos atribuicao | atribuicao |  
;
/*TA DANDO PAU A PARTIR DAQUI*/
atribuicao: variavel ATRIBUICAO expressao PONTO_E_VIRGULA

;

variavel:	{
				char str[15];
				TS *temp = busca_TS(token); printf("token variavel: %s\n", token);
				if(temp == NULL){
					fprintf(stderr, "Erro, simbolo nao esta na TS\n");
					exit(1);
				}
				sprintf(str, "ARMZ %d, %d", temp->nl, temp->desloc);
				geraCodigo (NULL, str);
			}
			IDENT
;

expressao: valor | constante
;

valor:	{		
			char str[15];
			TS *temp = busca_TS(token); printf("token valor: %s\n", token);
			if(temp == NULL){
				fprintf(stderr, "Erro, simbolo nao esta na TS\n");
				exit(1);
			}
			sprintf(str, "CRVL %d, %d", temp->nl, temp->desloc);
			geraCodigo (NULL, str);
		}

		  IDENT
;

constante: {
			char str[15];		
			sprintf(str, "CRCT %s", token);
			geraCodigo (NULL, str);
		  }
		  IDENT
;

%%

main (int argc, char** argv) {
   FILE* fp;
   extern FILE* yyin;

   if (argc<2 || argc>2) {
         printf("usage compilador <arq>a %d\n", argc);
         return(-1);
      }

   fp=fopen (argv[1], "r");
   if (fp == NULL) {
      printf("usage compilador <arq>b\n");
      return(-1);
   }


/* -------------------------------------------------------------------
 *  Inicia a Tabela de S�mbolos
 * ------------------------------------------------------------------- */

   yyin=fp;
   yyparse();

   return 0;
}


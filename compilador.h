/* -------------------------------------------------------------------
 *            Arquivo: compilador.h
 * -------------------------------------------------------------------
 *              Autor: Bruno Muller Junior
 *               Data: 08/2007
 *      Atualizado em: [15/03/2012, 08h:22m]
 *
 * -------------------------------------------------------------------
 *
 * Tipos, protótipos e variáveis globais do compilador
 *
 * ------------------------------------------------------------------- */

#define TAM_TOKEN 16

typedef enum simbolos { 
  simb_program, simb_var, simb_begin, simb_end, 
  simb_identificador, simb_numero,
  simb_ponto, simb_virgula, simb_ponto_e_virgula, simb_dois_pontos,
  simb_atribuicao, simb_abre_parenteses, simb_fecha_parenteses,
  simb_label, simb_type, simb_array, simb_of, simb_procedure, simb_function,
  simb_goto, simb_if, simb_then, simb_else, simb_while, simb_do, simb_not,
  simb_div, simb_and, simb_or,
} simbolos;

typedef struct tabela_simbolos {

	char id[50];
	char tipo[2];
	int nl, desloc;
	char var_tipo[10];
	struct tabela_simbolos *seguinte;

} TS;

typedef struct ts_pilha{
	TS *topo;
	int tamanho;

} TSP;


/* -------------------------------------------------------------------
 * variáveis globais
 * ------------------------------------------------------------------- */

extern simbolos simbolo, relacao;
extern char token[TAM_TOKEN];
extern int nivel_lexico;
extern int desloc;
extern int nl;
extern int num_vars;


simbolos simbolo, relacao;
char token[TAM_TOKEN];




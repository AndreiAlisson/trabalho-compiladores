
/* -------------------------------------------------------------------
 *            Aquivo: compilador.c
 * -------------------------------------------------------------------
 *              Autor: Bruno Muller Junior
 *               Data: 08/2007
 *      Atualizado em: [15/03/2012, 08h:22m]
 *
 * -------------------------------------------------------------------
 *
 * Fun��es auxiliares ao compilador
 *
 * ------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compilador.h"

TSP tsp;

void insere_TS(char token[], char tipo[], int nl, int desloc, char var_tipo[]){
	TS *t = malloc(sizeof(TS));
	t->seguinte = tsp.topo;
	tsp.topo = t;
	tsp.tamanho++;
	strcpy(t->id, token);
	strcpy(t->tipo, tipo);
	t->nl = nl;
	t->desloc = desloc;
	strcpy(t->var_tipo, var_tipo);
}

int remove_TS(){

	TS *temp = tsp.topo;
	tsp.topo = tsp.topo->seguinte;
	tsp.tamanho--;

	free(temp);

	return 1;
}

TS *busca_TS(char token[]){

	TS *temp = tsp.topo;

	while(temp){

		if(!strcmp(temp->id, token))
			return temp;

		temp = temp->seguinte;

	}

	return NULL;

}

void limpa_TS(){

	for(int i = 0; i < tsp.tamanho; i++)
		remove_TS();

}

/* -------------------------------------------------------------------
 *  vari�veis globais
 * ------------------------------------------------------------------- */

FILE* fp=NULL;
void geraCodigo (char* rot, char* comando) {

  if (fp == NULL) {
    fp = fopen ("MEPA", "w");
  }

  if ( rot == NULL ) {
    fprintf(fp, "     %s\n", comando); fflush(fp);
  } else {
    fprintf(fp, "%s: %s \n", rot, comando); fflush(fp);
  }
}

int imprimeErro ( char* erro ) {
  fprintf (stderr, "Erro na linha %d - %s\n", nl, erro);
  exit(-1);
}
